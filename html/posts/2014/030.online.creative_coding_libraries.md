# Open Source Libraries For Creative Coding

A collection of open source libraries we at [Apollo](http://www.apollomedia.nl) 
regurlarly use for our interactive simulations and installations. 

**Audio**

  - [libao](http://www.xiph.org/ao/doc/overview.html), simple audio output cross platform
  - [libmpg123](http://www.mpg123.de/api/), audio parsing (e.g. loading mp3 files)
  - [liblame](http://lame.sourceforge.net/), mp3 encoder
  - [libsndfile](http://www.mega-nerd.com/libsndfile/), audio file loading and writing
  - [opus](http://www.opus-codec.org/downloads/), the opus codec
  - [libsamplerate](http://www.mega-nerd.com/SRC/index.html), fast samplerate conversion

**Video**
     
  - [x264](http://www.videolan.org/developers/x264.html), best video encoding library around
  - [libvpx](http://www.webmproject.org/code/), video encoding/decoding

**I/O**

  - [libuv](https://github.com/joyent/libuv), threading, networking, file io, events
  - [nanomsg](http://nanomsg.org/), amazing networking, threading library
  - [libjansson](http://www.digip.org/jansson/), json reading/writing
  - [rapidxml](http://rapidxml.sourceforge.net/), xml reading/writing
  - [libcurl](http://curl.haxx.se/libcurl/), client-side url transfer library (ftp,http,etc..)

**Graphics**
        
  - [glfw](http://www.glfw.org/), openGL context + input library
  - [glxw](https://github.com/rikusalminen/glxw), openGL loader + linker
  - [anttweakbar](http://anttweakbar.sourceforge.net/), cross platform GUI 
  - [libharu](http://libharu.org/), library to create PDFs
  - [cairographics](http://cairographics.org/), 2D drawing 

**Geometry**

  - [psimpl](http://psimpl.sourceforge.net/index.html), generic n-dimensional polyline simplification