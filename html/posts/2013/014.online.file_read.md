# C++ file operation snippets

I always google for these snippets so I decided to put them all in one place; below
are some common c++ file operations.


_Open a file in binary mode_
````c++
std::ifstream ifs;
ifs.open("test.mov", std::ios::in | std::ios::binary);
if(!ifs.is_open()) {
  printf("Error trying to open the file.\n");
  ::exit(EXIT_FAILURE);
}
````

_Reading a binary file into an existing std::vector_
````c++

std::ifstream ifs;
ifs.open("test.mov", std::ios::in | std::ios::binary);

std::vector<char> buffer;
buffer.assign(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>());

ifs.close()
              
````

_Reading a text file into a string (note the extra parathesis for std::istreambuf_iterator)_
````c++
  std::ifstream ifs(filepath.c_str(), std::ios::in);
  if(!ifs.is_open()) {
    return false;
  }

  std::string str( (std::istreambuf_iterator<char>(ifs)) , std::istreambuf_iterator<char>());

````

_Reading a text file using pure C_

````c
int read_file(const char* path, char* buf, int len) {

  /* try to open the file */
  FILE* fp = fopen(path, "r");
  if(!fp) { 
    printf("Error: cannot read file: %s\n", path);
    return -1;
  }

  /* find size */
  long size = 0;
  fseek(fp, 0, SEEK_END);
  size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  if(size > len) {
    printf("Error: file size it too large for given buffer. We need: %ld bytes.\n", size);
    fclose(fp);
    return -2;
  }

  size_t r = fread(buf, size, 1, fp);
  if(r != 1) {
    printf("Error: cannot read file into buffer.\n");
    fclose(fp);
    return -3;
  }

  return (int)size;
}

````

_Check the file size_
````c++
ifs.seekg(0, std::ifstream::end);
size_t fsize = ifs.tellg();
printf("File size: %ld.\n", fsize);
````

_Jump to a position_
````c++
ifs.seekg(1024, std::ios_base::beg);     
if(!ifs) {
  printf("Error trying to jump to a position.\n");
  ::exit(EXIT_FAILURE);
}
````

_Get the curent position_
````c++
size_t pos = ifs.tellg();
printf("Current position: %ld.\n", pos);
````

_Jump to a absolute position_
````c++
ifs.seekg(0, std::ios_base::beg);
````

_Read some bytes_
````c++
const size_t buffer_size = 1024 * 1024 * 10;
char* buffer = new char[buffer_size];

size_t read_size = buffer_size;
if(fsize < read_size) {
  read_size = fsize;
  printf("The file is smaller then the number buffer.\n");
}

printf("Reading: %ld\n", read_size);
ifs.read(buffer, read_size);
if(!ifs) {
  printf("Error trying to into buffer.\n");
  exit(EXIT_FAILURE);
}

if(ifs.gcount() != sizeof(buffer)) {
  printf("Number of bytes is not the same as requested.\n");
}
printf("Read: %ld bytes into buffer\n", ifs.gcount());


delete[] buffer;
buffer = NULL;
````

_Read all bytes_

````c++
    do {
      ifs.read((char*)buffer, sizeof(buffer));
      nread = ifs.gcount();
      for (size_t i = 0; i < nread; ++i) {
        sprintf(hexbuf, "0x%02x", buffer[i]);
        ofs_source << hexbuf << ", ";
        ++nchars_written;
        if (nchars_written >= 16) {
          ofs_source << "\n  ";
          nchars_written = 0;
        }
      }
    }
    while (ifs.good());

````

_Write some bytes_
````c++

std::vector<char> buffer;
buffer.push_back('a');
buffer.push_back('b');
buffer.push_back('c');

std::ofstream ofs("myfile.bin", std::ios::bin | std::ios::out);
ofs.write(&buffer.front(), buffer.size();
ofs.close()

````

_Write the contents of a std::stringstream to a file_

````c++
std::ofstream ofs("data.txt", std::ios::out);
if(!ofs.is_open()) { 
  return;
}

std::stringstream ss;
ss << "some content.";

ofs << ss.rdbuf();

ofs.close();

````

And the complete source:

_Compile with:_
````sh
g++ main.cpp -g -o out && ./out
````

_Complete source_
````c++
#include <iostream>
#include <fstream>
#include <stdio.h>

int main() {

  // open a file in binary mode
  std::ifstream ifs;
  ifs.open("test.mov", std::ios::in | std::ios::binary);
  if(!ifs.is_open()) {
    printf("Error trying to open the file.\n");
    ::exit(EXIT_FAILURE);
  }

  // check the file size
  ifs.seekg(0, std::ifstream::end);
  size_t fsize = ifs.tellg();
  printf("File size: %ld.\n", fsize);


  // jump to a position from the start of the file
  ifs.seekg(1024, std::ios_base::beg);     
  if(!ifs) {
    printf("Error trying to jump to a position.\n");
    ::exit(EXIT_FAILURE);
  }

  // get current position
  size_t pos = ifs.tellg();
  printf("Current position: %ld.\n", pos);


  // go back to the beginning of the file.
  ifs.seekg(0, std::ios_base::beg);

  // read some bytes
  const size_t buffer_size = 1024 * 1024 * 10;
  char* buffer = new char[buffer_size];

  size_t read_size = buffer_size;
  if(fsize < read_size) {
    read_size = fsize;
    printf("The file is smaller then the number buffer.\n");
  }

  printf("Reading: %ld\n", read_size);
  ifs.read(buffer, read_size);
  if(!ifs) {
    printf("Error trying to into buffer.\n");
    exit(EXIT_FAILURE);
  }

  if(ifs.gcount() != sizeof(buffer)) {
    printf("Number of bytes is not the same as requested.\n");
  }
  printf("Read: %ld bytes into buffer\n", ifs.gcount());


  delete[] buffer;
  buffer = NULL;
  return 0;
}

````
