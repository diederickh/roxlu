{* -*- mode: html; -*- *}
<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <meta name="description" content="">
  <link rel="stylesheet" href="/css/roxlu.css">
  <link href='http://fonts.googleapis.com/css?family=Oxygen+Mono' rel='stylesheet' type='text/css'>  
  <link href='http://fonts.googleapis.com/css?family=PT+Serif+Caption' rel='stylesheet' type='text/css'>
  <script src="/js/mootools-core-1.4.5-full-nocompat-yc.js"></script>
  <script src="/js/roxlu.js"></script>
</head>
<body>
  <div id="about">
    Blog of roxlu, co-founder of <a href="http://www.apollomedia.nl">Apollo Media</a>. Contact info[shift+2]apollomedia.nl.
  </div>  
  <a href="http://www.apollomedia.nl"><img id="logo" src="/images/site/logo.png" alt="Apollo Media"></a>
  <div id="content">


    <div id="page">

      <ul id="post_links">
      {foreach from=$post_titles item=post}
      <li>
        <a href="{$post.link}">{$post.title}</a>
        </li>
      {/foreach}
      </ul>

      {$contents}

    </div>
  </div>

{literal}
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-579478-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
{/literal}

</body>
</html>
