<?php
require_once 'smarty/Smarty.class.php';
require_once $base_dir .'inc/md/Markdown.php';
require_once $base_dir .'inc/md/MarkdownExtra.php';
require_once $base_dir .'inc/geshi/geshi.php';

use \Michelf\MarkdownExtra;

/*
 When using this markdown we will replace the text with 
 geshi if you use e.g.

 ````c++
 [some code]
 ````
 */
function geshi_replace($matches) {
  if(count($matches) < 1) {
    return '';
  }

  $code = $matches[2];
  $lines = explode("\n", $code);
  if(!count($lines)) {
    return '';
  }

  $lang = trim($lines[0]);
  if(empty($lang)) {
    die('No language given for geshi, use ````c++ for example');
  }

  $lines[0] = '';
  $code = trim(implode($lines, "\n"));
  $geshi = new GeSHi($code, $lang);
  return $geshi->parse_code();
}

class Roxlu {
  var $config = NULL;
  var $smary = NULL;

  function __construct($config) {
    $this->config = $config;
    $this->smarty = new Smarty();
    $this->smarty->setTemplateDir($this->config['tpl_path']);
    $this->smarty->setCompileDir($this->config['cache_path']);
  }

  function execute($req) {
    $this->parse($req);
  }

  function parse($req) {
    $html = '';
    $post_titles = $this->getPostTitles($this->getPostList());

    if($req['path'][0] != '/') {
      $req['path'] = '/' .$req['path'];
    }

    if(is_array($req) && array_key_exists('path', $req)) {
      $html = $this->getPostByLink($req['path'], $post_titles);
    }
    
    $this->smarty->assign('post_titles', $post_titles);
    $this->smarty->assign('contents', $html);
    echo $this->smarty->fetch('index.tpl');
  }

  function getPosts($startIndex, $num = 1) {

    $posts = $this->getPostList();
    if(!is_array($posts)) {
      return '';
    }

    $end = $startIndex + $num;
    if($end > count($posts)) {
      $end = count($posts);
    }

    $html = '';
    for($i = $startIndex; $i < $end; ++$i) {

      $str = file_get_contents($posts[$i]);
      //preg_match_all('/````(.*)````/mis', $str, $out);
      //$out =  preg_split('/````(.*)````/mis', $str);
      $str = preg_replace_callback('/(````)(.*?)(````)/ism', "geshi_replace", $str,-1, $count);
      $html .= MarkdownExtra::defaultTransform($str);
    }
    return $html;
  }

  function getPostByLink($link, $postsTitles) {
    if(!$postsTitles) {
      return;
    }
    $content = '';
    foreach($postsTitles as $post) {
      if($post['link'] == $link) {
        $str = file_get_contents($post['file']);
        $str = preg_replace_callback('/(````)(.*?)(````)/ism', "geshi_replace", $str,-1, $count);
        $content =  MarkdownExtra::defaultTransform($str);
        break;
      }
    }
    return $content;
  }

  function getPostTitles($posts) {
    if(!is_array($posts)) {
      return '';
    }
    $result = array();
    foreach($posts as $post) {
      $str = file_get_contents($post);
      $lines = explode("\n", $str);
      $path = pathinfo($post);
      $fparts = explode('.', $path['filename']);
      if(!is_array($fparts)) {
        die('invalid filename: ' .$path['filename']);
      }
      if ($fparts[1] == 'offline') {
        continue;
      }
      $title =  trim(str_replace('#', '', $lines[0]));
      $link_title = preg_replace('/[^0-9a-zA-z]/', '-', strtolower($title));
      $result[] = array(
        'title' => $title
        ,'link' => '/' .basename($path['dirname']) .'/' .$fparts[0] .'/' .$link_title
        ,'link_title' => $link_title
        ,'file' => $post
      );
    }
    return $result;
  }

  function getPostList() {
    $dirs = array_filter(glob($this->config['posts_dir'] .'*'), 'is_dir');
    if(!is_array($dirs)) {
      die('Cannot find any post sub dirs in ' .$this->config['posts_dir'] .'. Create a subdir for each year');
    }

    $result = array();
    foreach($dirs as $dir) {
      $post_dir = $dir .'/';
      $files = array_filter(glob($post_dir .'*'), 'is_file');
      $result = array_merge($result, $files);
    }
    
    $result = array_reverse($result);
    return $result;
  }


  };