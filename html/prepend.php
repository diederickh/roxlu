<?php
session_start();

function p($a) { echo "<pre>"; print_r($a); echo "</pre>"; };
function e($s) { echo $s ."<br>"; } 

$path = pathinfo(__FILE__);
$base_dir = $path['dirname'] .'/';

ini_set('include_path', 
        ini_get('include_path')
        .':' .$base_dir .'inc/'
        .':' .$base_dir .'inc/markdown/Michelf/'
);

$config = array(
  'base_dir' => $base_dir
  ,'tpl_path' => $base_dir .'templates/'
  ,'cache_path' => $base_dir .'templates/cache/'
  ,'posts_dir' => $base_dir .'posts/'
);
